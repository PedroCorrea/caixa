# “SIMULAÇÃO SAQUE CAIXA ELETRÔNICO”
O projeto deverá contemplar, no mínimo,  as seguintes funcionalidades:
- Ao executar o programa, o caixa eletrônico deverá ser carregado com notas de 10, 20, 50 e 100 reais, sendo informado a quantidade de notas inseridas de cada um dos valores citados;
- Depois, deverá estar disponível para realização de saques sucessivos e informar a cada saque as notas que foram entregues a pessoa que está realizando aquele saque, sempre controlando o saldo e a quantidade de notas e expondo mensagens elucidativas quando não for possível o saque ;
- Em algum momento encerrar o funcionamento do “caixa eletrônico” exibindo o valor monetário ainda disponível  e as quantidades de notas que formam este valor.
