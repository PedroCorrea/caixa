#include <stdio.h>
#include <stdlib.h>

int ced[4][2];
int v = 0, dec[] = {100,50,20,10};

void limpa(int ced[4][2]);
void insere(int ced[4][2]);
void confere(int ced[4][2]);

int retira(int v, int i){
	if((ced[i][1] <= ced[i][0]) && (v >= dec[i]) && (i < 4)){  //verifica se a quantidade de notas na maquina sao suficientes | verifica se o valor e menor | verifica se ainda esta no tamanho da matriz
		ced[i][1]++;
		ced[i][0]--;
       	v = v - dec[i];
       	printf("R$%d - %d\n",dec[i], ced[i][1]);
       	printf("%d\n",v);
       	if(v >= dec[i]){
			retira(v,i);
		} else {
			retira(v,i+1);
		}
	} else {
		if((i < 4) && (v >= dec[i])){
			if(ced[i][0] == 0){
				printf("Notas insuficientes de %d\n",dec[i]);
			}
            retira(v,i+1); //retorna o resto q n pode ser divisivel pela nota
		} else {
			if(v >= dec[1]){
            	v = retira(v,1);
        	} else if(v >= dec[2]){
            	v = retira(v,2);
        	} else if(v <= dec[3] && ced[3][0] == 0){
            	return v;
        	} else {
				v = retira(v,3);
			}	
		}
	}
	//return v;
}

void saque(){
    int x = 1;
    while(x == 1){ //inicia repeticao para saque
        system("cls"); //windows = system("cls"); ubuntu = system("clear")
        printf("BEM VINDO AO CAIXA ELETRONICO\n\n");
        printf("Digite o valor para ser sacado: ");
        scanf("%d",&v);
        if(v >= dec[0]){
            v = retira(v,0);
        } else if(v >= dec[1]){
            v = retira(v,1);
        } else if(v >= dec[2]){
            v = retira(v,2);
        } else if(v >= dec[3]){
            v = retira(v,3);
        }

        if(v != 0){ //mostra o resto q n pode ser sacado cm os valores
            printf("Nao foi possivel sacar R$%d\n",v);
        }

        printf("\n\n"); //comentar essas 4 linhas
        confere(ced);

        printf("\n\nContinuar? (1 - s/0 - n)\n"); //pergunta se o usuario qr sair ou continuar sacando
        scanf("%d",&x);
    }
}

int main(){
    limpa(ced);
    printf("BEM VINDO AO CAIXA ELETRONICO\n\n");
    printf("Favor, inserir cedulas:\n");

    //preenchendo a matriz cm as cedulas
    insere(ced);

    //chamando a funcao de repeticao para saques
    saque();
    return 0;
}

void limpa(int ced[4][2]){ //limpa a matriz caso tenha lixo na memoria
	int i, j;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 2; j++){
            ced[i][j] = 0;
        }
    }
}

void insere(int ced[4][2]){ //insere a cedulas na primeira coluna da matriz
	int i;
    for(i = 0; i < 4; i++){
        printf("R$%d - ",dec[i]);
        scanf("%d",&ced[i][0]);
    }
}

void confere(int ced[4][2]){ //mostra a matriz
	int i, j;
    for(i = 0; i < 4; i++){
        for(j = 0; j < 2; j++){
            printf("%d ",ced[i][j]);
        }
        printf("\n");
    }
}
